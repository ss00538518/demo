package com.example.gg00539076.demo;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by GG00539076 on 1/24/2018.
 */
public class MainActivityTest {

    MainActivity mActivity;

    @Before
    public void setUp() throws Exception {
        mActivity = new MainActivity();
    }

    @Test
    public void onCreate() throws Exception {
    }

    @Test
    public void add() throws Exception {
        assertEquals(7, mActivity.add(4, 3));
    }

}